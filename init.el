;; -*- coding: utf-8  -*-
(setq gc-cons-threshold most-positive-fixnum)
(setq package-archives '(("gnu"   . "https://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
			 ("nongnu" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/nongnu/")
                         ("melpa" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
			 ("melpa-stable" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/stable-melpa/")
			 ))
(package-initialize) ;; You might already have this line
(if (not package-archive-contents)
    (package-refresh-contents))
(setq custom-file "~/.emacs.d/locals.el")
(set-fontset-font t 'chinese-gbk (font-spec :family "等距更纱黑体 SC") nil)
(load "~/.emacs.d/tweaks.el")
(load "~/.emacs.d/package-config.el")
;(load "~/.emacs.d/org-config.el")
(load "~/.emacs.d/locals.el")
(set-fontset-font "fontset-default" 'emoji "Segoe UI Emoji" nil 'prepend)

