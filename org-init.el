;(setq org-modules '(org-habit org-protocol org-mouse org-crypt org-tempo org-id ol-info ol-eshell)) No longer working (when using use-package)
(setq org-modules-loaded t)
;; DEPRECATED
;(with-eval-after-load 'org
  ;(require 'org-protocol)
  ;(define-key org-mode-map (kbd "C-c b") 'org-switchb)
					;  )
(use-package org-mouse
  :after org)
(use-package org-tempo
  :after org)
(use-package org-crypt
  :after org
  :config
  (org-crypt-use-before-save-magic)
  (setq org-crypt-tag-matcher "crypted")
  (setq org-tags-exclude-from-inheritance '("crypted"))
  (setq org-crypt-key "1B46BF71D0679DCD055D16DC5812746E55676A69")
  (setq org-crypt-disable-auto-save nil)
  )
(use-package org-protocol
  :after org)
