(use-package plantuml-mode
  :ensure t
  :defer t
  :pin melpa-stable
  :config
  (setq plantuml-jar-path "~/.emacs.d/others/plantuml.jar")
  (setq plantuml-exec-mode 'jar))
;;(use-package yaml-mode
;;  :ensure t)
(use-package dashboard
  :ensure t
  :pin melpa-stable
  :config
  (setq dashboard-projects-backend 'project-el)
  (setq dashboard-items '((recents . 5)
			  (bookmarks . 5)
			  (projects . 5)
	(registers . 5)))
  (dashboard-setup-startup-hook))
(use-package solarized-theme
  :ensure t
  :pin melpa-stable)
(use-package dracula-theme
  :ensure t
  :pin nongnu)
(use-package modus-themes
  :ensure t
  :init
  (setq modus-themes-to-toggle '(modus-operandi-tinted modus-vivendi-tinted))
  (setq modus-themes-headings '((0 . (1.3))
				(1 . (1.3))
				(2 . (1.2))
				(3 . (1.15))
				(4 . (1.1))
				)))
(use-package ef-themes
  :ensure t
  :init
  (setq ef-themes-to-toggle '(ef-reverie ef-dream))
  (setq ef-themes-headings '((0 . (1.3))
				(1 . (1.3))
				(2 . (1.2))
				(3 . (1.15))
				(4 . (1.1))
				)))
(use-package sr-speedbar
  :ensure t
  :after org
  :bind ("<f6>" . sr-speedbar-toggle)
  )
(use-package ox-gfm
  :after ox
  :defer t
  :ensure t
  :pin melpa-stable)
(use-package ox-pandoc
  :after ox
  :defer t
  :ensure t
  :pin melpa-stable)
;;(use-package adoc-mode
;;  :ensure t
;;  :defer t
;;  :pin melpa-stable)
(use-package denote
  :ensure t)
(use-package org
  :defer t
  :hook (org-mode . imenu-add-menubar-index)
  :autoload org-clock-persistence-insinuate
  :init
  (load "~/.emacs.d/org-init.el")
  
  :config
  (load "~/.emacs.d/org-config.el"))
